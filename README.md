# Pedestrians First in Africa

**Data from ITDP's _Pedestrians First_ project to measure and map indicators of walkability in cities globally.**

_Pedestrians First_ uses open-source data, mostly from [OpenStreetMap](https://openstreetmap.org) and the [Global Human Settlement Layer](https://ghsl.jrc.ec.europa.eu/), to measure walkability anywhere on earth. This dataset on Digital Transport for Africa includes measurements and maps of five indicators for 65 large cities in Africa with urban agglomeration populations above 1 million.

It includes these indicators:
1. The percentage of the population living within a 1-kilometer walk of both some form of healthcare and some form of education.
2. The percentage of the population living within a 500-meter walk of frequent public transit (only available in a handful of cities).
3. The block density, the average number of city blocks per square kilometer, which is a proxy for pedestrian network connectivity.
4. The weighted population density.
5. The percentage of the population living within 100 meters of a car-free open space or park.

To quickly and easily view the data for a particular city, visit [pedestriansfirst.itdp.org](https://pedestriansfirst.itdp.org) and use the _View City Measurements_ tool.

To view and compare the summary indicators for each of the 65 cities, see the _Pedestrians_First_indicators_for_DT4A.csv_ file. To download geospatial data for a particular city, download the relevant .zip file from the _city_data/_ directory. The numbers in filenames are the HDC codes used to uniquely identify cities in the Global Human Settlement Layer dataset. 

To learn about the methods used to generate this data, visit [pedestriansfirst.itdp.org/methods](https://pedestriansfirst.itdp.org/methods).

With any questions, contact [taylor.reich@itdp.org](mailto:taylor.reich@itdp.org).
